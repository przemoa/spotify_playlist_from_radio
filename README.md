# Spotify Playlist from Radio

A simple RUST application to create a Spotify playlist from songs played in a radio.

Song titles are scrapped from the radio tracking service, which allows to check songs played at specific time.
Scrapping this data (for songs played in 2022 in radio `RMF Classic`) allowed to create a playlist with over 2500 songs.

<img src="./docs/playlist_image.png"  height="350" >


# Development and running

## Application and radio selection
Set the required radio station ID, dates and parameters in `app_config.rs`.

## Spotify configuration
To obtain spotify credentials:
- create project at: https://developer.spotify.com/dashboard/applications/b3986b7bedd746ca97c8e651fc2f81ee
- find `Client ID` and `Client Secret` on the app page and put the in `.env` file
