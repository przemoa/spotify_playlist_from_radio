use std::{path::PathBuf, str::FromStr, time::Duration};

use chrono::Utc;

#[derive(Clone, Debug)]
pub struct AppConfig {
    pub log4rs_config_path: PathBuf, // Path to the log4rs config file
    pub start_date: chrono::NaiveDateTime,
    pub end_date: chrono::NaiveDateTime,
    pub radio_station_id: u8,

    pub songs_list_file_path: PathBuf,
    pub skip_download: bool, // if you wish to use only the list from file

    pub track_ids_file_path: PathBuf,
    pub skip_searching_track_ids: bool, // if you wish to use only the list from file
}

impl AppConfig {
    /// Get the default application configuration for normal program run
    pub fn default() -> Self {
        Self {
            log4rs_config_path: PathBuf::from("config/log4rs.yml"),
            start_date: chrono::NaiveDate::from_ymd_opt(2022, 2, 1)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap(),
            end_date: chrono::NaiveDate::from_ymd_opt(2022, 10, 1)
                .unwrap()
                .and_hms_opt(0, 0, 0)
                .unwrap(),
            radio_station_id: 6,
            songs_list_file_path: PathBuf::from("songs_list.json"),
            skip_download: false,
            track_ids_file_path: PathBuf::from("track_ids.json"),
            skip_searching_track_ids: false,
        }
    }
}
