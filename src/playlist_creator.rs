use crate::app_config::AppConfig;
use rspotify::model::idtypes;
use rspotify::prelude::*;
use rspotify::{
    clients::pagination::Paginator,
    model::{
        AlbumId, ArtistId, CurrentPlaybackContext, Device, EpisodeId, FullPlaylist, ItemPositions,
        Offset, PlayableId, PlaylistId, RecommendationsAttribute, RepeatState, ShowId, TimeLimits,
        TimeRange, UserId,
    },
    prelude::*,
    ClientResult, Token,
};
use rspotify::{
    model::{Country, Market, SearchResult, SearchType, TrackId},
    prelude::*,
    scopes, AuthCodeSpotify, ClientCredsSpotify, Credentials, OAuth,
};

pub struct PlaylistCreator {
    app_config: AppConfig,
    spotify: AuthCodeSpotify,
}

impl PlaylistCreator {
    pub fn new(app_config: &AppConfig) -> Self {
        let creds = Credentials::from_env().unwrap();

        let scopes = scopes!(
            "playlist-modify-public",
            "playlist-modify-private",
            "playlist-read-private",
            "playlist-read-collaborative",
            "user-read-private"
        );

        let oauth = OAuth::from_env(scopes).unwrap();
        let mut spotify = AuthCodeSpotify::new(creds, oauth);
        spotify.config.token_cached = true;

        let url = spotify.get_authorize_url(false).unwrap();
        log::info!("Open this URL in your browser:\n{}\n", url);
        log::info!("Or wait until it happens automatically. Then paste the ENTIRE URL in console and press enter");

        // This function requires the `cli` feature enabled.
        spotify.prompt_for_token(&url).unwrap();

        log::info!("Authorization OK");

        Self {
            app_config: app_config.clone(),
            spotify,
        }
    }

    fn get_track_id_for_song(&mut self, song_title: &str) -> Option<TrackId> {
        // let track_query = "Georg-Philipp Telemann - Koncert Na Trąbkę D-Dur (4)";
        // let track_query = "Marek Grechuta - Dni, Których Jeszcze Nie Znamy";
        let track_query = song_title;

        let result = self
            .spotify
            .search(
                track_query,
                &SearchType::Track,
                Some(Market::Country(Country::Poland)).as_ref(),
                None,
                Some(1),
                None,
            )
            .ok()?;

        let track = match result {
            SearchResult::Tracks(tracks) => tracks,
            _ => {
                println!("Not a track");
                return None;
            }
        };

        if track.items.is_empty() {
            return None;
        }

        let popularity = track.items[0].popularity;
        let track_id = track.items[0].id.clone()?;
        println!("{}: popularity {} [{}]", song_title, popularity, &track_id);
        Some(track_id)
    }

    fn download_track_ids(&mut self, songs_list: &Vec<String>) -> Vec<TrackId> {
        let mut track_ids = Vec::new();

        let mut i = 0;
        for song in songs_list {
            i += 1;
            // if i > 3 {
            //     break;
            // }

            let track_id = match self.get_track_id_for_song(song) {
                Some(track_id) => track_id,
                None => {
                    println!("{}: Failed to get track id!", song);
                    continue;
                }
            };
            track_ids.push(track_id);
        }

        track_ids
    }

    pub fn create_playlist(&mut self, songs_list: &Vec<String>) {
        let mut track_ids;

        if self.app_config.skip_searching_track_ids {
            println!("Skipping searching track ids");
            // load track ids from json
            let track_ids_json = std::fs::read_to_string(&self.app_config.track_ids_file_path)
                .expect("Failed to read track ids json file");
            track_ids = serde_json::from_str(&track_ids_json).unwrap();
        } else {
            track_ids = self.download_track_ids(songs_list);
            // save track ids to file as json
            let track_ids_json = serde_json::to_string_pretty(&track_ids).unwrap();
            std::fs::write("track_ids.json", track_ids_json).unwrap();
        }

        log::info!("Number of tracks with duplicates: {}", track_ids.len());
        // remove duplicates
        track_ids.sort_by(|a, b| a.to_string().cmp(&b.to_string()));
        track_ids.dedup();
        log::info!("Number of tracks without duplicates: {}", track_ids.len());

        let user = self.spotify.me();
        let user_id = user.unwrap().id;

        log::info!("Creating playlist...");

        let playlist = self
            .spotify
            .user_playlist_create(
                &user_id,
                "test",
                Some(true),
                Some(false),
                Some("Test playlist"),
            )
            .expect("Failed to create playlist");

        log::info!("Adding tracks to playlist...");
        // add tracks to playlist with 100 tracks per request

        for chunk in track_ids.chunks(50) {
            log::info!("Adding chunk of tracks to playlist...");
            let playable_id_list = chunk.iter().map(|track| track as &dyn PlayableId);
            self.spotify
                .playlist_add_items(&playlist.id, playable_id_list, None)
                .expect("Error adding tracks to playlist!");
        }

        log::info!("Playlist created: {}", playlist.external_urls["spotify"]);
    }
}
