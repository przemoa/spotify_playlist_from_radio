// Just for early development, as everything is changing rapidly
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_imports)]

use crate::app_config::AppConfig;
use crate::playlist_creator::PlaylistCreator;
use crate::radio_downloader::RadioDownloader;

mod app_config;
mod playlist_creator;
mod radio_downloader;
mod tmpp;

fn init_logger(app_config: &AppConfig) {
    log4rs::init_file(&app_config.log4rs_config_path, Default::default())
        .expect("Failed to initialize logger");
}

fn main() {
    dotenv::dotenv().expect(".env file not found!");
    let app_config = AppConfig::default();
    init_logger(&app_config);
    log::info!("Application started!");

    let mut radio_downloader = RadioDownloader::new(&app_config);
    radio_downloader.download_songs_list();
    let songs_list = radio_downloader.get_songs_list();

    let mut playlist_creator = PlaylistCreator::new(&app_config);
    playlist_creator.create_playlist(songs_list);

    log::info!("Application finished!");
}
