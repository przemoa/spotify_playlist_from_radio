use crate::app_config::AppConfig;
use chrono::{Datelike, Timelike};
use scraper::{Html, Selector};

pub struct RadioDownloader {
    app_config: AppConfig,
    songs_list: Vec<String>,
}

impl RadioDownloader {
    pub fn new(app_config: &AppConfig) -> Self {
        let mut instance = Self {
            app_config: app_config.clone(),
            songs_list: Vec::new(),
        };
        instance.load_from_file();
        instance
    }

    pub fn get_songs_list(&self) -> &Vec<String> {
        &self.songs_list
    }

    pub fn load_from_file(&mut self) {
        if self.app_config.songs_list_file_path.exists() {
            let file_content = std::fs::read_to_string(&self.app_config.songs_list_file_path)
                .expect("Failed to read songs list file");
            let songs_list: Vec<String> =
                serde_json::from_str(&file_content).expect("Failed to deserialize songs list file");
            self.songs_list.extend(songs_list);
        }
    }

    fn get_songs_from_url(&mut self, url: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let mut response = reqwest::blocking::get(url)?;
        let response_text = response.text()?;

        // let response_text = std::fs::read_to_string("/home/przemek/Desktop/radio.html")?;
        // println!("Status: {}", response.status());

        let document = Html::parse_document(&response_text);

        let selector = Selector::parse("a.title-link").unwrap();
        let mut songs_list = Vec::new();

        for element in document.select(&selector) {
            let song_name = element
                .text()
                .collect::<Vec<_>>()
                .join("")
                .trim()
                .to_string();

            let forbidden_names = vec![
                "Reklama w serwisie".to_string(),
                "Radia".to_string(),
                "Polityka prywatności".to_string(),
            ];
            if forbidden_names.contains(&song_name) {
                continue;
            }

            println!("Song name: {}", song_name);
            songs_list.push(song_name);
        }

        Ok(songs_list)
    }

    pub fn download_songs_list(&mut self) {
        if self.app_config.skip_download {
            return;
        }

        log::info!("Downloading songs list...");

        let mut t = self.app_config.start_date;
        while t <= self.app_config.end_date {
            let start_hour = t.hour();
            t += chrono::Duration::hours(2);
            let mut end_hour = t.hour();
            if end_hour >= 24 {
                end_hour = 0;
            }

            log::info!("Downloading songs from {t}");

            let url = format!(
                "https://www.odsluchane.eu/szukaj.php?r={}&date={}&time_from={}&time_to={}",
                self.app_config.radio_station_id,
                t.format("%d-%m-%Y"),
                start_hour,
                end_hour
            );

            // sleep for 100 ms to not overload the server
            std::thread::sleep(std::time::Duration::from_millis(100));

            match self.get_songs_from_url(&url) {
                Ok(songs) => {
                    log::info!("Found {} songs", songs.len());
                    self.songs_list.extend(songs);
                }
                Err(e) => {
                    log::error!("Error while downloading songs list: '{}'", e);
                    continue;
                }
            };
        }

        self.remove_duplicates_and_save_to_file();
    }

    fn remove_duplicates_and_save_to_file(&mut self) {
        // remove duplicated songs
        self.songs_list.sort();
        self.songs_list.dedup();

        let json = serde_json::to_string_pretty(&self.songs_list)
            .expect("Failed to serialize songs list to JSON");
        std::fs::write(&self.app_config.songs_list_file_path, json)
            .expect("Failed to write songs list to file");

        log::info!("Saved {} songs to file", self.songs_list.len());
    }
}
